
package org.nlu.forest.buffaloes.apps;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class App {
	
	public static boolean sendMail(final Map<String, String> smtpConfig, final String userName, final String password, String mailSender, String subject, String content, String customerMail) throws MessagingException, IOException {
		Properties props = new Properties();
		
		for (Map.Entry<String, String> prop : smtpConfig.entrySet()) {
			props.put(prop.getKey(), prop.getValue());
		}
		
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});
		
		Message message = new MimeMessage(session);
		message.setHeader("Content-Type", "text/plain; charset=UTF-8");
		message.setFrom(new InternetAddress(mailSender));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(customerMail));
		MimeBodyPart bodypart = new MimeBodyPart();
		bodypart.setHeader("Content-Type","text/plain; charset=\"utf-8\"");
		bodypart.setContent( content, "text/plain; charset=utf-8" );
		bodypart.setHeader("Content-Transfer-Encoding", "quoted-printable");
		Multipart multipart = new MimeMultipart();
		
		// add the message body to the mime message
		multipart.addBodyPart( bodypart );
		message.setSubject(subject);
		// Put all message parts in the message
		message.setContent( multipart );
		Transport.send(message);
		
		return true;
	}
	
	private static void init() throws MessagingException, IOException{
		
		String emailTemplate = "Hello";
        String emailSubject =  "Test Mail";
        String emailSender =  "media.hcmuaf@gmail.com";
        String userName = "media.hcmuaf@gmail.com";
        String password =  "thankwhy";
        
        Map<String, String> resourceBundleMap =  new HashMap<String, String>();
        Map<String, String> replacedProperties =  new HashMap<String, String>();
        Map<String, String> smtpConfig =  new HashMap<String, String>();
        smtpConfig.put("smtp", "smtp.gmail.com");
        smtpConfig.put("username", "media.hcmuaf@gmail.com");
        smtpConfig.put("password", "thankwhy");
        smtpConfig.put("port", "587");

        String content = replacement(emailTemplate, replacedProperties, resourceBundleMap);
        String customerMail = "xuantungcb3@gmail.com";
        
        sendMail(smtpConfig, userName, password, emailSender, emailSubject, content, customerMail);
		
	}
	
	public static void main(String[] args) throws MessagingException, IOException {
		
		//Create and set up the window. 
		JFrame frame = new JFrame("Mail Application");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		
		JLabel textLabel = new JLabel("I'm a label in the window",SwingConstants.CENTER); 
		textLabel.setPreferredSize(new Dimension(300, 100)); 
		frame.getContentPane().add(textLabel, BorderLayout.CENTER); 
		
		//Display the window. 
		frame.setLocationRelativeTo(null); 
		frame.pack();
		frame.setVisible(true); 
	}
	
	private static String replacement (String template, Map<String, String> replacedProperties, Map<String, String> map) {
		
		if (template == null){
			return null;
		}
		
		String retValue = template;
		
		for(String key : replacedProperties.keySet()){
			retValue = retValue.replaceAll(getReplacementKey(key), replacedProperties.get(key));
		}
		
		for(String key : map.keySet()){
			String value = map.get(key);
			retValue = retValue.replaceAll(getReplacementKey(key), value);
		}
		
		return retValue;
	}
	
	
	private static String getReplacementKey (String key) {
		return "@@@" + key + "@@@";
	}
}